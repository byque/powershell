# Imprimir el primer argumento.
# Basado en:
# https://www.red-gate.com/simple-talk/sysadmin/powershell/how-to-use-parameters-in-powershell/

Write-Host $args[0]